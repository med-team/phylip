Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PHYLIP
Upstream-Contact: Joe Felsenstein <joe@gs.washington.edu>
Source: http://evolution.gs.washington.edu/phylip/download/
Files-Excluded: *.exe
                *.jar
                *.so
                src/buildDmg.sh
                src/mac
                src/javajars
                src/icons
                src/Makefile.osx
                src/Makefile.cyg
                src/javasrc/util/CVS

Files: *
Copyright: 1980-2014 Joe Felsenstein <joe@gs.washington.edu>,
                     Jerry Shurman, Hisashi Horino, Akiko Fuseki,
                     Sean Lamont, and Andrew Keeffe
License: BSD-2-clause

Files: debian/*
Copyright: 1998-1999 Stephane Bortzmeyer <bortzmeyer@pasteur.fr>
           2001 Dr. Guenter Bechly <gbechly@debian.org>
           2001-2014 Andreas Tille <tille@debian.org>
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
