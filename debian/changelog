phylip (1:3.697+dfsg-4) unstable; urgency=medium

  * Team upload.
  * fix-declarations.patch: new: fix a build failure. (Closes: #1066595)
  * d/*.lintian-overrides: fix mismatches.
  * d/watch: use secure uri.

 -- Étienne Mollier <emollier@debian.org>  Sat, 16 Mar 2024 19:43:07 +0100

phylip (1:3.697+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.6.2 (routine-update)
  * Marking the -doc package as M-A: foreign
  * Marking two patches as Forwarded: not-needed

 -- Pierre Gruet <pgt@debian.org>  Sun, 03 Dec 2023 22:59:56 +0100

phylip (1:3.697+dfsg-2) unstable; urgency=medium

  * Team upload

  [ Steffen Möller ]
  * Added ref to conda
  * d/u/metadata: yamllint

  [ Shayan Doust ]
  * Fix gcc-10 multiple definition errors (Closes: #957681)
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.

 -- Shayan Doust <hello@shayandoust.me>  Thu, 23 Jul 2020 16:37:36 +0000

phylip (1:3.697+dfsg-1) unstable; urgency=medium

  * Fix watch file

  [ Steffen Moeller ]
  * d/upstream/metadata: Added references to registries.

  [ Andreas Tille ]
  * New upstream version
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Secure URI in copyright format
  * Drop useless get-orig-source target
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/control
  * Do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Mon, 29 Oct 2018 11:44:41 +0100

phylip (1:3.696+dfsg-5) unstable; urgency=medium

  * Bioperl-run expects executable named Consense
  * d/rules: Does not reliably build with debhelper compat level 10
    automatic --parallel feature so set --no-parallel

 -- Andreas Tille <tille@debian.org>  Fri, 16 Dec 2016 16:02:40 +0100

phylip (1:3.696+dfsg-4) unstable; urgency=medium

  * Bioperl-run expects executables named DrawGram and DrawTree.  Since
    this might be expected by other tools to, the spelling is adjusted
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Fri, 16 Dec 2016 14:15:48 +0100

phylip (1:3.696+dfsg-3) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * hardening=+all
  * install bash completions to usr/share/bash-completion/completions
  * fix java wrappers

  [ Canberk Koç ]
  * autopkgtest added

 -- Canberk Koç <canberkkoc@gmail.com>  Sat, 23 Jul 2016 17:21:18 +0300

phylip (1:3.696+dfsg-2) unstable; urgency=medium

  * Add missing optimisation flags (Thanks to Atanas Kumbarov
    <kumbarov@gmail.com> for the patch)
    Closes: #783354
  * cme fix dpkg-control
  * Delete unused lintian override
  * d/watch: Fix version mangling

 -- Andreas Tille <tille@debian.org>  Sun, 26 Apr 2015 21:48:52 +0200

phylip (1:3.696+dfsg-1) unstable; urgency=medium

  * New upstream version now with free license
  * cme fix dpkg-control
  * removed redundant README.source
  * d/copyright:
     - DEP5 now with BSD-2-Clause license
     - exclude some binaries without source which are not needed
  * Add bash_completion

 -- Andreas Tille <tille@debian.org>  Wed, 17 Sep 2014 19:35:06 +0200

phylip (1:3.695-1) unstable; urgency=low

  * New upstream version
  * debian/control:
     - use anonscm in Vcs fields
     - cme fix dpkg-control
     - debhelper 9
     - debian/source/format: 3.0 (quilt)
  * Upstream changed build system - this chance was used to switch from
    cdbs to dh (patches were changed)
  * Lintian overrides

 -- Andreas Tille <tille@debian.org>  Wed, 07 Aug 2013 22:17:03 +0200

phylip (1:3.69-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Fixed Vcs-Svn (missing svn/)
    - Updated Standards-Version to 3.8.3 (README.source)
    - Droped [Biology] from short description
  * Debhelper 7

 -- Andreas Tille <tille@debian.org>  Fri, 11 Sep 2009 08:00:10 +0200

phylip (1:3.68-2) unstable; urgency=low

  * debian/control: Added myself to Uploaders. Vcs-* fields now point to
    trunk.
  * Simplified build process by removing 01_Makefile.patch and adjusting
    debian/rules. Removed commented out code from debian/rules.
  * Added a patch that prevents treedist from writing empty files on amd64.

 -- Manuel Prinz <manuel@debian.org>  Sat, 01 Nov 2008 17:19:54 +0100

phylip (1:3.68-1) unstable; urgency=low

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 17 Aug 2008 11:40:34 -0300

phylip (1:3.67-3) unstable; urgency=low

  [ Andreas Tille ]
  * Added XS-Autobuild: yes to enable auto building
  * Standards-Version: 3.8.0 (no changes needed)
  * debian/README.Debian: s?usr/doc?usr/share/doc?

  [ Charles Plessy ]
  * Changed the doc-base section according to the new policy.

 -- Andreas Tille <tille@debian.org>  Tue, 22 Jul 2008 15:51:33 +0200

phylip (1:3.67-2) unstable; urgency=low

  * Group maintenance:
    - Maintainer: Debian-Med Packaging Team
      <debian-med-packaging@lists.alioth.debian.org>
    - XS-DM-Upload-Allowed: Yes
    - Uploaders: Andreas Tille <tille@debian.org>
    - Vcs-Browser: http://svn.debian.org/wsvn/debian-med/trunk/packages/phylip
    - Vcs-Svn: svn://svn.debian.org/debian-med/trunk/packages/phylip
  * Standards-Version: 3.7.3 (no changes needed)
  * Homepage: http://evolution.genetics.washington.edu/phylip.html
  * Conversion of debian/copyright to UTF8
  * Make use of quilt instead of simplepatchsys

 -- Andreas Tille <tille@debian.org>  Tue, 12 Feb 2008 18:29:32 +0100

phylip (1:3.67-1) unstable; urgency=low

  * New upstream version
  * Added watch file
  * No need for auto_update debian/control so removed this
  * phylip-doc package in section doc

 -- Andreas Tille <tille@debian.org>  Mon, 03 Sep 2007 09:47:35 +0200

phylip (1:3.66-1) unstable; urgency=low

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri,  1 Sep 2006 23:30:58 +0200

phylip (1:3.65-1) unstable; urgency=low

  * New upstream version
    Closes: #367237
  * Switched to cdbs
  * Debhelper 5
  * Standards-Version: 3.7.2
  * Fixed Copyright statement (Thanks to Charles Plessy)
    Closes: #381140

 -- Andreas Tille <tille@debian.org>  Thu,  3 Aug 2006 21:17:58 +0200

phylip (1:3.6.1-2) unstable; urgency=low

  * Added Build-Depends: libx11-dev, libxt-dev, libxaw7-dev
    Thanks to Andreas Jochens <aj@andaco.de>
    Closes: #266650

 -- Andreas Tille <tille@debian.org>  Wed, 18 Aug 2004 18:57:27 +0200

phylip (1:3.6.1-1) unstable; urgency=low

  * New upstream version
  * Moved from non-free/misc to section non-free/science
  * Switched to debhelper 4
  * Standards-Version: 3.6.1
  * Added doc-base
  * New version comes with extensive HTML documentation which was split up
    to separate Debian package

 -- Andreas Tille <tille@debian.org>  Tue, 13 Jul 2004 12:00:17 +0200

phylip (3.573c-7) unstable; urgency=low

  * Corrected spelling bug (Thanks to Matt Zimmerman <mdz@debian.org>)
    closes: #125245

 -- Andreas Tille <tille@debian.org>  Tue, 18 Dec 2001 11:56:36 +0100

phylip (3.573c-6) unstable; urgency=low

  * Forgot to close wnpp-bug so this upload
    closes: #100251
  * Added URL to the package description because I consider this as
    "good style" to have an upstream link without installing the package
  * Added PhylipGuide document which can be found at
    http://www.es.embnet.org/~pprpc/activs/PHYLIPGuide/PhylipGuide-1.6.html

 -- Andreas Tille <tille@debian.org>  Tue, 30 Oct 2001 08:06:24 +0100

phylip (3.573c-5) unstable; urgency=low

  * New Maintainer
  * A patch by Patrice Godard <pgodard@pasteur.fr> fo fix #67657 is
    applied because it compiles well for me in contrast to 3.573c-2
    where it was enclosed as patch.diff.
    closes: #67657
  * Standards-Version: 3.5.6

 -- Andreas Tille <tille@debian.org>  Tue, 23 Oct 2001 12:35:14 +0200

phylip (3.573c-4) unstable; urgency=low

  * Maintainer set to Debian QA Group <packages@qa.debian.org>.

 -- Adrian Bunk <bunk@fs.tum.de>  Fri, 24 Aug 2001 23:41:36 +0200

phylip (3.573c-3) unstable; urgency=low

  * Phylip manpage completely revised.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Mon, 16 Apr 2001 19:21:10 +0200

phylip (3.573c-2) unstable; urgency=low

  * Package adopted by new maintainer; closes: #92789
  * Moved manpage to /usr/share/man; closes: #91028
  * Moved last file from /usr/doc to /usr/share/doc; closes: #91601
  * Updated to latest policy, and added Build-Depends in control.
  * A patch by Patrice Godard <pgodard@pasteur.fr> fo fix #67657 is enclosed as
    patch.diff but was not applied since it failed to compile.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat,  7 Apr 2001 16:00:14 +0200

phylip (3.573c-1) unstable; urgency=low

  * The wrapper displays the name of available programs. Closes #33382
  * Switch to dh_make
  * New and better description.
  * New upstream release. Closes #36411. Patches in bug report #36475 does
    not seem to have been integrated upstream :-( So, I patched myself.
    Closes #36475.

 -- Stephane Bortzmeyer <bortzmeyer@debian.org>  Tue, 18 May 1999 17:45:10 +0200

phylip (3.5c-3) unstable; urgency=low

  * Bug in dnapars fixed (incompatible with glibc).
  * New "tests" directory with samples.
  * Fonts now included (they were forgotten)

 -- Stephane Bortzmeyer <bortzmeyer@debian.org>  Wed, 21 Oct 1998 17:01:02 +0200

phylip (3.5c-2) unstable; urgency=low

  * The many executables are replaced by a wrapper. First public release.

 -- Stephane Bortzmeyer <bortzmeyer@debian.org>  Thu, 10 Sep 1998 10:43:11 +0200

phylip (3.5c-1) unstable; urgency=low

  * Initial Release.

 -- Stephane Bortzmeyer <bortzmeyer@pasteur.fr>  Fri, 28 Aug 1998 14:51:10 +0200

Local variables:
mode: debian-changelog
End:
